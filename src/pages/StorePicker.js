import React, { Component } from 'react'
import { getFunName } from '../lib/helpers'

export default class StorePicker extends Component {
  
  goToStore(e) {
    e.preventDefault();
    const storeId = this.storeInput.value
    this.context.router.transitionTo(`/store/${storeId}`)
  }

  render() {
    return (
      <form className="store-selector" onSubmit={this.goToStore.bind(this)}>
        <h2>Please Enter A Store</h2>
        <input 
          type="text" 
          required
          placeholder="Store Name" 
          defaultValue={getFunName()} 
          ref={ input => this.storeInput = input }/>
        <button type="submmit"> Visit store  → </button>
      </form>
    )
  }
}

StorePicker.contextTypes = {
  router: React.PropTypes.object
}