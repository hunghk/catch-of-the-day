import React, { Component } from 'react'
import Header    from '../components/Header'
import Order     from '../components/Order'
import Inventory from '../components/Inventory'
import Fish      from '../components/Fish'
import orderStore    from '../stores/Order'
import fishesStore   from '../stores/Fishes'
import { observer } from 'mobx-react'
import { database } from '../db/firebase'

@observer
class App extends Component {
  componentWillMount() {
    const { storeId } = this.props.params
    this.fishesStore = fishesStore.getFishes(storeId)
  }

  componentWillUnmount() {
    const { storeId } = this.props.params
    database.ref(`${storeId}/fishes`).off()
  }

  render() {
    const fishes = this.fishesStore.fishes
    const { storeId } = this.props.params

    return (
      <div className="catch-of-the-day">
        <div className="menu">
          <Header age="5000" cool={true} tagline="Fresh Seafood Market" />
          <ul className="list-of-fishes">
            { fishes && Object.keys(fishes).map(f => <Fish key={f} details={fishes[f]} storeId={storeId} index={f} addToOrder={() => orderStore.addToOrder(f)}/>)}
          </ul>
        </div>
        <Order
          fishes={fishes}
          storeId={storeId}
          />
        <Inventory
          storeId={storeId}
          fishesStore={this.fishesStore}
          />
      </div>
    )
  }
}

export default App;