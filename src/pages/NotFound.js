import React from 'react'

const NotFound = (props) => {
  return (
    <div className="catch-of-the-day">
      <h2> Not Found </h2>
    </div>
  )
}

export default NotFound;