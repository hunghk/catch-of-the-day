import { observable } from 'mobx'
import { database } from '../../firebase'

class Fishes {
  @observable fishes = {}

  constructor(storeId) {
    this.storeId = storeId

    database.ref(`${storeId}/fishes`).on('value', (snapshot) => {
      if (snapshot.val() !== null) {
        this.fishes = snapshot.val()
      }
    })
  }

  updateFish(fishId, fish) {
    this.fishes = {
      ...this.fishes,
      [fishId]: fish
    }
    database.ref(`${this.storeId}/fishes`).child(fishId).update(fish)
  }

  addFish(fish) {
    const timestamp = Date.now()
    const fishId = `fish-${ timestamp }`
    this.fishes = {
      ...this.fishes,
      [fishId]: fish
    }
    database.ref(`${this.storeId}/fishes/${fishId}`).set(fish)
  }

  removeFish(key) {
    const fishes = {...this.fishes}
    delete fishes[key]
    this.fishes = fishes
    database.ref(`${this.storeId}/fishes/${key}`).remove()
  }

  loadSamples(sampleFishes) {
    database.ref(`${this.storeId}/fishes`).set(sampleFishes)
  }
}

class FishesFactory {
  stores = {}

  getFishes(storeId) {
    if (!this.stores[storeId]) {
      this.stores[storeId] = new Fishes(storeId)
    }

    return this.stores[storeId]
  }
}

const fishesFactory = new FishesFactory()
export default fishesFactory