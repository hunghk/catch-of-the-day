import { observable } from 'mobx'

class OrderStore{
  @observable _order = {}
  @observable _storeId = null
  addToOrder(key){
    if (this.order[key]) {
      this.order[key]++
    }else {
      this.order = {
        ...this.order,
        [key]: 1
      }
    }
  }

  removeFromOrder(key) {
    if (this._order[key])
      delete this._order[key]
    this._order = {...this._order}
  }

  get order(){
    return this._order
  }

  set order(order){
    this._order = order

    if (this._storeId){
      localStorage.setItem(`order-${this._storeId}`, JSON.stringify(order))
    }
  }  

  get storeId(){
    return this._storeId
  }

  set storeId(storeId){
    this._storeId = storeId
    const localStorageRef =  localStorage.getItem(`order-${storeId}`)
    if (localStorageRef)
      this._order = JSON.parse(localStorageRef)
  }
}

export default new OrderStore();