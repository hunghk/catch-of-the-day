import React, { Component } from 'react';
import { formatPrice } from '../lib/helpers';
import orderStore from '../stores/Order'
import { observer } from 'mobx-react'

@observer
class Fish extends Component {

  render() {
    const { image, name, price, desc, status } = this.props.details
    const { index } = this.props
    const isAvailable = status === 'available'
    const btnText     = isAvailable ? 'Add To Order' : 'Sold Out'
    const store = orderStore.getStore(this.props.storeId)

    return (
    <li className='menu-fish'>
      <img src={image} alt={name} />
      <h3 className='fish-name'>
        {name}
        <span className='price'>{formatPrice(price)}</span>
      </h3>
      <p>{desc}</p>
      <button onClick={store.addToOrder.bind(store,index)} disabled={!isAvailable}> {btnText} </button>
    </li>
    );
  }
}


Fish.propTypes = {
  details: React.PropTypes.object.isRequired,
  addToOrder: React.PropTypes.func.isRequired,
  index: React.PropTypes.string.isRequired,
  storeId: React.PropTypes.string.isRequired,
}


export default Fish