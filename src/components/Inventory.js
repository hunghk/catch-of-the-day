import React, { Component } from 'react'
import AddFishForm from './AddFishForm'
import firebase from 'firebase/app'
import { auth, database } from '../db/firebase'
import sampleFishes from '../db/seeds/fishes'

export default class Inventory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uid: null,
      owner: null,
    }
  }

  renderInventory(key, fish) {
    const props = {}
    props.onChange = (e) => this.handleChanges(e, key)

    return(
      <div className="fish-edit" key={key}>
        <input type="text" name="name" value={fish.name} placeholder="Fish Name" {...props} />
        <input type="number" name="price" value={fish.price} placeholder="Fish Price" {...props}/>
        <select value={fish.status} name="status" {...props}>
          <option value="available">Fresh!!</option>
          <option value="unavailable">Sold Out</option>
        </select>
        <textarea name="desc" placeholder="Fish Desc" value={fish.desc} {...props}></textarea>
        <input name="image" type="text" value={fish.image} placeholder="Fish Image" {...props}/>
        <button onClick={this.fishesStore.removeFish.bind(this.fishesStore, key)}>Remove</button>
      </div>
    )
  }

  renderLogin(){
    return(
      <nav className="login">
        <h2> Inventory</h2>
        <p> Sign in manage toy store's inventory </p>
        <button className="github"   onClick={() => this.authenticate(new firebase.auth.GithubAuthProvider())}>Login Github ↵</button>
        <button className="facebook" onClick={() => this.authenticate(new firebase.auth.FacebookAuthProvider())}>Login facebook ↵</button>
        <button className="twitter"  onClick={() => this.authenticate(new firebase.auth.TwitterAuthProvider())}>Login twitter ↵</button>
      </nav>
    )
  }

  onLogout(){
    auth.signOut()
    this.setState({
      uid: null
    })
  }

  componentWillMount() {
    this.fishesStore = this.props.fishesStore

    auth.onAuthStateChanged((user) => {
      if (user){
        this.authHandler({user})
      }
    })
  }

  authHandler(authData) {
    const storeRef = database.ref(this.props.storeId)
    storeRef.once('value', (snapshot) => {
      const data = snapshot.val() || {}
      if (!data.owner) {
      storeRef.set({
        owner: authData.user.uid
      })}

      this.setState({
        uid: authData.user.uid,
        owner: data.owner || authData.user.uid
      })
    })
  }

  authenticate(provider){
    auth.signInWithPopup(provider).then((authData) => this.authHandler(authData))
  }

  handleChanges(e, key){
    const fish = this.fishesStore.fishes[key]
    const updateFish = {
      ...fish,
      [e.target.name]: e.target.value
    }

    this.fishesStore.updateFish(key, updateFish)
  }

  renderBottom(){
    return(
      <div>
        <AddFishForm addFish={this.fishesStore.addFish.bind(this.fishesStore)}/>
        <button onClick={this.fishesStore.loadSamples.bind(this.fishesStore, sampleFishes)}>Load Sample Fishes</button>
        <button onClick={this.onLogout.bind(this)}>Logout ↝ </button>
      </div>
    )
  }

  render() {
    if (!this.state.uid)
      return <div>{this.renderLogin()}</div>
  
    if (this.state.uid !== this.state.owner) {
      return (
        <div>
          <h2>Inventory 🐟</h2>
          <p> Sorry you are not the owner of this store! </p>
          <button onClick={this.onLogout.bind(this)}>Logout ↝ </button>
        </div>
      )
    }
    const fishes = this.fishesStore.fishes
    return (
      <div>
        <h2>Inventory 🐟</h2>
        { Object.keys(fishes).map(f => this.renderInventory(f, fishes[f])) }
        {this.renderBottom()}
      </div>
    )
  }
}

Inventory.propTypes = {
  storeId: React.PropTypes.string.isRequired,
  fishesStore: React.PropTypes.object.isRequired
}