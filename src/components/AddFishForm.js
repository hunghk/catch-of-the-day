import React, { Component } from 'react'

export default class AddFishForm extends Component {
  createFish(e){
    e.preventDefault()
    const fish = {
      name: `🐟 ${this.name.value}`,
      price: parseFloat(this.price.value),
      status: this.status.value,
      desc: this.desc.value,
      image: this.image.value
    }

    this.props.addFish(fish)
    this.fishForm.reset()
  }

  render() {
    return (
      <form ref={form => this.fishForm = form} className="fish-edit" onSubmit={(e) =>this.createFish(e)}>
        <input required ref={input => this.name = input } type="text" placeholder="Fish Name" />
        <input required ref={input => this.price = input } type="number" min="0" step="0.1" placeholder="Fish Price" />
        <select ref={input => this.status = input } >
          <option value="available">Fresh!!</option>
          <option value="unavailable">Sold Out</option>
        </select>
        <textarea ref={input => this.desc = input } placeholder="Fish Desc" ></textarea>
        <input ref={input => this.image = input } type="text" placeholder="Fish Image" />
        <button type="submit">+ Add Item</button>
      </form>
    )
  }
}


AddFishForm.propTypes = {
  addFish: React.PropTypes.func.isRequired
}