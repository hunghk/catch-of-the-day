import React, { Component } from 'react'
import { formatPrice }      from '../lib/helpers';
import CSSTransitionGroup   from 'react-addons-css-transition-group'
import { observer } from 'mobx-react'
import orderStore from '../stores/Order'

@observer
class Order extends Component {
  renderOrder(key) {
    const { fishes } = this.props
    const fish = fishes[key]
    const count = this.store.order[key]
    const btnRemove = <button onClick={() => this.store.removeFromOrder(key)}>&times;</button>

    if (!fish || fish.status === 'unavailable')
      return <li key={key}>Sorry, {fish ? fish.name : 'fish'} is no longer available! {btnRemove}</li>

    return(
      <li key={key}>
        <span>
          <CSSTransitionGroup
            component="span"
            className="count"
            transitionName="count"
            transitionEnterTimeout={100}
            transitionLeaveTimeout={100}
            >
            <span key={count}>{count} </span>
          </CSSTransitionGroup>
           lbs {fish.name}
        </span>
        <span className='price'>{formatPrice(count * fish.price)}</span>
        {btnRemove}
      </li>
    )
  }

  componentWillMount() {
    this.store = orderStore.getStore(this.props.storeId)
  }

  render() {
    const { fishes} = this.props
    const orderIds = Object.keys(this.store.order);
    const total = orderIds.reduce((preTotal, key) => {
      const fish = fishes[key]
      const count = this.store.order[key]
      const isAvailable = fish && fish.status === 'available'
      if (isAvailable)
        return preTotal + (count * fish.price || 0)
      return preTotal
    }, 0)

    return (
      <div className='order-wrap'>
        <h2> Order </h2>
        <CSSTransitionGroup
          className="order"
          component="ul"
          transitionName="order"
          transitionEnterTimeout={1000}
          transitionLeaveTimeout={1000}
          >
          {orderIds.map(this.renderOrder.bind(this))}
          <li className='total'>
            <strong> Total: </strong>
            {formatPrice(total)}
          </li>
        </CSSTransitionGroup>
      </div>
    )
  }
}

Order.propTypes = {
  fishes: React.PropTypes.object.isRequired,
  storeId: React.PropTypes.string.isRequired
}


export default Order